#!/usr/bin/python
#
# Read a list of phrases, write Markov chain data.
#
# $Id$

import cPickle as pickle

def getLines(filename):
    return [line[0:-1] for line in open(filename).readlines()]

def getWords(lines):
    words = []
    for line in lines:
        words.extend(line.split())
    return words

def createProbabilityHash(words):
    numWords = len(words)
    wordCount = {}
    for word in words:
        if wordCount.has_key(word):
            wordCount[word] += 1
        else:
            wordCount[word] = 1
 
    for word in wordCount.keys():
        wordCount[word] /= 1.0 * numWords
    return wordCount

def createMarkovChain(filename, outfile):
    words = getWords(getLines(filename))

    wordMap = {}
    previous = (words[0], words[1])
    for word in words[2:]:
        if wordMap.has_key(previous):
            wordMap[previous].append(word)
        else:
            wordMap[previous] = [word]
        previous = (previous[1], word)

    for word in wordMap.keys():
        probabilityHash = createProbabilityHash(wordMap[word])
        wordMap[word] = probabilityHash

    fd = open(outfile, "w")
    pickle.dump(wordMap, fd, pickle.HIGHEST_PROTOCOL)
    fd.close()


if __name__ == "__main__":
    import sys
    if len(sys.argv) != 3:
        print >>sys.stderr, "Usage: %s <INFILE> <OUTFILE>" % sys.argv[0]
	sys.exit(1)
    createMarkovChain(sys.argv[1], sys.argv[2])

'''
Manages hackmeeting self-service web application
'''

import cgi
import cPickle as pickle
import random
import os.path


def find_dats(path):
    for f in sorted(os.listdir(path)):
        if f.endswith('.dmp'):
            yield os.path.splitext(f)[0], f


DATADIR = '.'
DATASETS = dict(find_dats(DATADIR))
DEFAULT_PROFILE = sorted(DATASETS.keys())[0]

TEMPLATE = '''
<html>
<head>
 <title>hackmeeting ml self service</title>
 <link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body><div id="main"><div id="ph">%(phrases)s</div><p id="footer">
  <a href="/hmbot/">random</a> |
  <a href="/hmbot/?p=%(profile)s&s=%(seed)d">permalink</a> |
  personality: %(profiles)s
<!-- Piwik -->
<a href='http://piwik.org' title='Web analytics' onclick='window.open(this.href);return(false);'>
<script language='javascript' src='http://stats.autistici.org/piwik.js' type='text/javascript'></script>
<script type='text/javascript'>
<!--
piwik_action_name = document.title;
piwik_idsite = 1222;
piwik_url = 'http://stats.autistici.org/piwik.php';
piwik_log(piwik_action_name, piwik_idsite, piwik_url);
//-->
</script><object>
<noscript><p>Web analytics <img src='http://stats.autistici.org/piwik.php' style='border:0' alt='piwik'/></p>
</noscript></object></a>
<!-- /Piwik -->
</p></div>
</body>
</html>
'''

def render(phrases, seed, profile):
    phtml = []
    for p in phrases:
        phtml.append("<p>%s</p>" % p)
    profiles = []
    dk = DATASETS.keys()
    dk.sort()
    for p in dk:
        profiles.append("<a href=\"?p=%s\">%s</a>" % (p, p))
    return TEMPLATE % dict(phrases="".join(phtml), seed=seed,
                           profile=profile, profiles=" | ".join(profiles))

def getRandomWord(r, wordCount):
    randomValue = r.random()
    cumulative = 0.0
    for word in wordCount:
        cumulative += wordCount[word]
        if cumulative > randomValue:
            return word

def isCaps(x):
    if x[0][0] in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
        return True
    return False

# Load and initialize markov chain data for all datasets.
wordMaps = {}
startChoices = {}
for key, filename in DATASETS.items():
    fd = open(filename, "rb")
    wordMaps[key] = pickle.load(fd)
    startChoices[key] = filter(isCaps, wordMaps[key].keys())
    fd.close()

# WSGI application
def markov_app(environ, start_response):
    r = random.Random()
    form = cgi.parse_qs(environ["QUERY_STRING"])
    if "s" in form:
        seed = int(form["s"][0])
    else:
        seed = r.getrandbits(32)
    r.seed(seed)
    if "p" in form:
        profile = form["p"][0]
    else:
        profile = DEFAULT_PROFILE
    word_map = wordMaps[profile]
    start_choices = startChoices[profile]
    previous = r.choice(start_choices)

    # Generate 3 phrases, or a maximum of 1000 words.
    num_words = 1000
    num_phrases = 3
    phrases = []
    curp = [previous[0], previous[1]]
    for i in xrange(num_words):
        word = getRandomWord(r, word_map[previous])
        curp.append(word)
        if word.endswith("."):
            phrases.append(" ".join(curp))
            num_phrases -= 1
            if num_phrases == 0:
                break
            previous = r.choice(start_choices)
            curp = [previous[0], previous[1]]
        else:
            previous = (previous[1], word)

    data = render(phrases, seed, profile)
    hdrs = [("Content-Length", "%d" % len(data)),
            ("Content-Type", "text/html; charset=utf-8")]
    start_response("200 OK", hdrs)
    return [data]

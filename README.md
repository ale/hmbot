HowTo
=====

Preparing data
--------------

```sh
virtualenv --no-site-packages ve
source ve/bin/activate
pip install -r requirements.txt
```

* get an interesting mbox, say `my.mbox`
* filter it with `python scanna.py my.mbox > filtered.mbox`
* build a chain with `python create_chain.py filtered.mbox filtered.dmp`

Now, you are ready to have fun.

Irc Bot
-------

`python irc_bot.py filtered.dmp mychannelname`

no, you should NOT put the `#` in front of mychannelname

Web Application
---------------

```sh
gunicorn hm_app:markov_app
```

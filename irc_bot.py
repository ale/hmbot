#!/usr/bin/python
# hmbot IRC interface
# $Id$

import sys
import random
import re
import cPickle as pickle
import os

from twisted.words.protocols import irc
from twisted.internet import protocol
from twisted.internet import reactor

def maiuscolo(x):
    if x[0][0] in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
        return True
    return False

def load_dataset(filename):
    # on-the-fly lowercase all keys
    fd = open(filename, "rb")
    words = {}
    starters = []
    data = pickle.load(fd)
    for k, v in data.iteritems():
        if maiuscolo(k):
            k = (k[0].lower(), k[1].lower())
            starters.append(k)
        words[k] = v
    fd.close()
    return words, starters

stopWords = set([x.strip() for x in open("stopwords_it.txt")])
def removeStopWords(s):
    w = filter(lambda x: x.lower() in stopWords, s.split())
    return " ".join(w)

keywords = {}
def extract_keywords(wordpairs):
    for wp in wordpairs:
        for w in wp:
            w = w.lower()
            if w in stopWords:
                continue
            keywords.setdefault(w, []).append(wp)

def find_keyword(words):
    for kw in words:
        kw = kw.lower()
        if kw in keywords:
            return random.choice(keywords[kw])

def get_seed(words):
    words = map(lambda x: x.lower(), words)
    words = filter(lambda x: x not in stopWords, words)
    random.shuffle(words)
    if words:
        seed = find_keyword(words)
        if seed:
            return seed
    return random.choice(startChoices)

def getRandomWord(wordCount):
    randomValue = random.random()
    cumulative = 0.0
    for word in wordCount:
        cumulative += wordCount[word]
        if cumulative > randomValue:
            return word

class MomBot(irc.IRCClient):
    def _get_nickname(self):
        return self.factory.nickname
    nickname = property(_get_nickname)

    def talkingToMe(self, msg):
        if msg.startswith(self.nickname):
            return True
        if self.nickname in msg.split():
            return True
        return False

    def signedOn(self):
        self.join(self.factory.channel)

    def privmsg(self, user, channel, msg):
        if not user or not channel:
            return
        if random.random() > 0.03 and not self.talkingToMe(msg):
            return
        msg = re.sub(r"^%s[:, ]" % self.nickname, "", msg)
        #prefix = "%s: " % (user.split("!", 1)[0], )
        prefix = ""
        sentence = self.generate(msg)
        self.msg(self.factory.channel, prefix + sentence)

    def generate(self, msg):
        max_words = 50
        msg = msg.strip().lower()
        msg = re.sub(r"[^-a-z0-9'_]", " ", msg)
        curp = list(get_seed(msg.split()))
        previous = tuple(curp)
        for i in xrange(max_words):
            lprev = (previous[0].lower(), previous[1].lower())
            try:
                word = getRandomWord(wordMap[lprev])
            except KeyError:
                break
            curp.append(word)
            if word.endswith("."):
                break
            previous = (previous[1], word)
        return " ".join(curp)

class MomBotFactory(protocol.ClientFactory):
    protocol = MomBot

    def __init__(self, channel, nickname='hmbot'):
        self.channel = channel
        self.nickname = nickname

    def clientConnectionLost(self, connector, reason):
        print "Lost connection (%s), reconnecting." % (reason,)
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print "Could not connect: %s" % (reason,)


if __name__ == "__main__":
    global wordMap, startChoices
    wordMap, startChoices = load_dataset(sys.argv[1])
    print "loaded Markov map (size=%d, seeds=%d)" % (
        len(wordMap), len(startChoices))
    extract_keywords(wordMap.keys())
    chan = sys.argv[2]
    if not chan.startswith('#'):
        chan = '#' + chan
    if len(sys.argv) > 3:
        nick = sys.argv[3]
    else:
        nick = os.path.splitext(sys.argv[1])[0]
    reactor.connectTCP('ai.irc.mufhd0.net', 6667,
            MomBotFactory(chan, nick))
    reactor.run()


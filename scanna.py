#!/usr/bin/python
#
# Scan a mailbox for messages from a specific sender, then cleans
# up the email bodies and outputs just the plain text on stdout.
#
# $Id$

import email
import mailbox
import optparse
import re
import sys

def debug(s):
    print >>sys.stderr, s

def unicodize(s):
    for enc in ['ascii', 'utf-8', 'iso-8859-1']:
        try:
            return unicode(s, enc)
        except UnicodeDecodeError:
            continue
    return s

def get_msg_text(m):
    out = []
    for part in m.walk():
        if part.get_content_maintype() == 'text':
            out.append(part.get_payload(decode=True))
    return ''.join(out)

def scan_mbox(filename, sender_nick):
    sender_pattern = re.compile(sender_nick + "@")
    phrases = []
    debug("scanning %s for messages from '%s'" % (filename, sender_pattern))
    mbox = mailbox.UnixMailbox(open(sys.argv[1], "r"), email.message_from_file)
    num_msgs = 0
    for msg in mbox:
        if not sender_pattern.search(msg["From"]):
            continue
	# Avoid forwards
	if re.search(r'fwd?:', msg["Subject"].lower()):
	    continue
        num_msgs += 1
        text = unicodize(get_msg_text(msg))
        out = []
        for line in text.split("\n"):
            line = line.strip()
	    # Skip PGP signed message headers
            if line.startswith("-----BEGIN PGP SIGNED MESSAGE") or line.startswith("Hash:"):
                continue
	    # Skip signatures
            if line.startswith("- --") or line.startswith("--") or line.startswith("__"):
                break
	    # Skip reply text
            if line.startswith(">") or line.startswith("|"):
                continue
	    # Crappy reply-text header detector
            if line.endswith("wrote:") or line.endswith("ha scritto:") or line.endswith("rigurgitava:") or line.endswith("scrisse:") or line.endswith("disse:"):
                continue
            if not line:
                continue
	    # Skip some common useless lines
            if line == "ciao":
                continue
            out.append(line)
        if out:
	    # Split the text in phrases, do some cleanup
            msgtext = " ".join(out).replace("\n", " ")
            for pattern, repl in [(r'[\(\)"]', " "),
                                  (r'[?!.]\s+', ".\n"),
                                  (r'[:;]\)', ".\n"),
				  (r'\s+:\s', ": "),
                                  (r' +', " ")]:
                msgtext = re.sub(pattern, repl, msgtext)
            curp = map(lambda y: y.strip(), msgtext.split("\n"))
	    curp = filter(lambda x: x, curp)
	    curp = filter(lambda x: not re.search("=====", x), curp)
            phrases += curp
    debug("found %d messages" % num_msgs)
    return phrases


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print >>sys.stderr, "Usage: %s <MBOX> <FROM_REGEX>" % sys.argv[0]
	sys.exit(1)
    phrases = scan_mbox(sys.argv[1], sys.argv[2])
    for p in phrases:
        if not p.endswith("."):
            p += "."
        print p.capitalize().encode('utf-8')


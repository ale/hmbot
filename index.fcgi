#!/usr/bin/python
# $Id$

from hm_app import markov_app


if __name__ == "__main__":
    from flup.server.fcgi import WSGIServer
    WSGIServer(markov_app).run()
